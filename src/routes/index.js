const express = require('express');

const usersRouter = require('./users');
const authRouter = require('./auth');
const AWSUploadsRouter = require('./awsUploads');
// const clinicHistoriesRouter = require('./clinicHistories');
// const examsRouter = require('./exams');
// const laboratoriesRouter = require('./laboratories');
// const readFileRouter = require('./readFile');
// const generatePDFRouter = require('./generatePDF');

const routes = (app) => {
  const router = express.Router();

  app.use('/users', usersRouter);
  app.use('/', authRouter);
  app.use('/upload', AWSUploadsRouter);
  // app.use('/clinic-histories', clinicHistoriesRouter);
  // app.use('/exams', examsRouter);
  // app.use('/laboratories', laboratoriesRouter);
  // app.use('/read-file', readFileRouter);
  // app.use('/read-file', readFileRouter);
  // app.use('/pdf', generatePDFRouter);

  app.use('/', router);
  router.get('/', async (req, res) => {
    res.send('Hello world!');
  });
};

module.exports = routes;
